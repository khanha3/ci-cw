import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;

public class Particle {

    private double[] position;
    private double[] velocity;

    private double[] personalBest;

    private double bestcost;

    private static double[] globalBest;


    public Particle(double[] position, double[] velocity, PricingProblem problem) {
        this.position = position;
        Arrays.sort(this.position);
        this.velocity = velocity;
        Arrays.sort(this.velocity);
        this.personalBest = position;
        Arrays.sort(this.personalBest);

        double[] b = getPersonalBest();
       // b[b.length -1] = (double) problem.bounds()[0][1]/2;

    }

    public void updatePostion()
    {
        position = addVector(position , velocity);
    }

    public void updateVelocity(PricingProblem problem)
    {
      double[] cograndom = multiplyVector(randomvector(problem),cognativeattraction());
      double[] sorandom  = multiplyVector(randomvector(problem),socialattraction());

      double[] socialcongnativeadd = addVector( cograndom , sorandom);

      double[] intertiaadd = addVector(socialcongnativeadd , intertia());

      velocity = intertiaadd.clone();
    }


    private double[] addVector(double[] a , double[] b )
    {
        double[] sum = new double[a.length];
        for (int i =0 ; i <= a.length -1 ; i++)
        {
            sum[i] = a[i] + b[i];
        }
        return sum;
    }

    private double[] subVector(double[] a , double[] b )
    {
        double[] min = new double[a.length];
        for (int i =0 ; i <= a.length-1 ; i++)
        {
            min[i] = a[i] - b[i];
        }
        return min;
    }
    private double[] multiplyVector(double[] a , double[] b )
    {
        double[] product = new double[a.length];
        for (int i =0 ; i <= a.length-1 ; i++)
        {
            product[i] = round(a[i] * b[i] , 2);
        }
        return product;
    }

    private double[] intertia()
    {
        double[] intert = velocity;
        for(int i = 0 ; i<= velocity.length -1 ; i++)
        {
            intert[i] = velocity[i] * 0.721;
        }
        return intert;
    }

    private double[] cognativeattraction()
    {
       return subVector(personalBest,position);

    }

    private double[] socialattraction()
    {
        return subVector(globalBest , position);
    }

    private double[] randomvector(PricingProblem problem)
    {
        double[] random = new double[problem.getN()];

        for (int i = 0 ; i <= problem.getN() -1 ; i++)
        {
            random[i] = Math.random() * 1.1193;
        }
        return random;

    }

    public double eval(PricingProblem problem)
    {
       // personalBest[personalBest.length-1] = (double) problem.getN()/2;

        return problem.evaluate(personalBest);
    }

    public void updatePersonalBest(PricingProblem problem)
    {
        double[] a = getPersonalBest();
       // a[a.length-1] = (double) problem.getN()/2;

        double[] b = getPosition();
        //b[b.length-1] = (double) problem.getN()/2;


        if (problem.evaluate(b) > problem.evaluate(a))
        {
            personalBest = getPosition();
        }
    }



    public double[] getPosition() {
        return position;
    }

    public double[] getVelocity() {
        return velocity;
    }

    public double[] getPersonalBest() {
        return personalBest;
    }

    public static void setGlobalBest(double[] globalBest , PricingProblem problem) {

        Particle.globalBest = globalBest;
    }

    public void updateGlobalBest( PricingProblem problem)
    {
        double[] b = personalBest;


        double[] a = globalBest;

        if (problem.evaluate(b) > problem.evaluate(a))
        {
            globalBest = personalBest;
        }
    }

    public double[] getGlobalBest() {
        return globalBest;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
