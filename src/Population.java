public class Population {

    private Individual[] individuals;
    private int populationSize;


    public Population(int populationSize, PricingProblem p, boolean initialise) {
        this.populationSize = populationSize;
        individuals = new Individual[populationSize];
        // Initialise population
        if (initialise) {
            // Loop and create individuals
            for (int i = 0; i < populationSize; i++) {
                Individual newIndividual = new Individual(p);

                saveIndividual(i, newIndividual);
            }
        }
    }

    public void saveIndividual(int index, Individual indiv) {
        individuals[index] = indiv;
    }
    public Individual[] getIndividuals() {
        return individuals;
    }

    public Individual getIndividual(int index) {
        return individuals[index];
    }

    public Individual getfittest()
    {
        Individual fittest = individuals[0];

        for ( Individual i : individuals)
        {
            if ( i.getFitness() > fittest.getFitness() )
            {
                fittest = i;
            }
        }

        return fittest;
    }

    public Individual getSecondfittest()
    {

        Individual fittest = individuals[0];
        Individual secondfittest = individuals[0];

        for ( Individual i : individuals)
        {
            if (i.getFitness() > fittest.getFitness() )
            {
                secondfittest = fittest;
                fittest = i;
            }
            else if (i.getFitness() > secondfittest.getFitness())
            {
                secondfittest = i;
            }
        }

        return secondfittest;
    }


    public int getPopulationSize() {
        return populationSize;
    }
}
