public class Individual {


    private double[] genes;
    private double fitness;

    public Individual(PricingProblem p) {
       this.genes = randSolution(p);
       this.fitness = p.evaluate(this.genes);

    }


    public static double[] randSolution(PricingProblem p) {

        int size = p.getN();
        double[] t = new double[size];

        double max = 10.0;
        double min = 0.01;

        while (true) {
            for (int i = 0; i <= t.length - 1; i++) {
                t[i] = (Math.random() * (max - min) - 1) + min;
            }
            if (p.is_valid(t)) {
                return t;
            }

        }
    }

    public int size() {
        return genes.length;
    }

    public double getGene(int index) {
        return genes[index];
    }

    public void setGene(int index, double value) {
        genes[index] = value;
    }


    public double getGenesFitness(PricingProblem p)
    {
       return p.evaluate(genes);
    }

    public double[] getGenes() {
        return genes;
    }


    public double getFitness() {
        return fitness;
    }


}
