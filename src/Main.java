import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

            PricingProblem f = PricingProblem.randomInstance(20);

//            double[] bob = swarm(f);
//            //System.out.println(Arrays.toString(bob));
//            System.out.println(f.evaluate(bob));
//


            double[] bob = evolution(f, 50000);
            //System.out.println(Arrays.toString(bob));
            System.out.println(f.evaluate(bob));

    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static double[] randSolution(PricingProblem A) {

        int noant = A.getN();
        double[] t = new double[noant];
        double[][] b = A.bounds();


        double max = 10.0;
        double min = 0.01;

        while (true) {
            for (int i = 0; i <= t.length - 1; i++) {
                t[i] = round((Math.random() * (max - min) - 1) + min, 2  );
            }
            if (A.is_valid(t)) {
                return t;
            }

        }
    }

    public static double[] swarm(PricingProblem A)
    {
        Particle[] swarm = new Particle[100];

        for (int i=0 ; i < swarm.length ; i++)
        {
            double[] a = randSolution(A);
            double[] b = randSolution(A);

            swarm[i] = new Particle(a,b,A);
        }
        double[] a = randSolution(A);
        double[] b = randSolution(A);
        double[] c = randSolution(A);
        Particle p = new Particle( a ,b  ,A);

        p.setGlobalBest(c , A);

        long t= System.currentTimeMillis();
        long end = t+10000;
        while(System.currentTimeMillis() < end) {

//            for ( Particle i : swarm) {
//                i.updateVelocity(A);
//                i.updatePostion();
//                i.eval(A);
//                i.updateGlobalBest(A);
//                i.updatePersonalBest(A);
//            }
            Arrays.stream(swarm).parallel().forEach(particle -> { particle.updateVelocity(A);particle.updatePostion();particle.eval(A);particle.updateGlobalBest(A);particle.updatePersonalBest(A);});

            //System.out.println(A.evaluate(p.getGlobalBest()));

        }

        return swarm[0].getGlobalBest();
    }

    public static double[] evolution(PricingProblem A , int steps )
    {
        Population  population = new Population(20 , A , true);

        for (int i = 0 ; i < steps ; i++)
        {
            population =  Eva.evolvePopulation(population,A);
            System.out.println(A.evaluate(population.getfittest().getGenes()));
            //System.out.println(Arrays.toString(population.getfittest().getGenes()));
        }
        return population.getfittest().getGenes();
    }

}
