import java.math.BigDecimal;
import java.math.RoundingMode;

public class Eva {

    private static final double uniformRate = 0.5;
    private static final double mutationRate = 0.01;
    private static final int tournamentSize = 5;
    private static final boolean elitism = true;



    public static Population evolvePopulation(Population popo ,PricingProblem p) {
        Population newPopulation = new Population(popo.getPopulationSize(),p ,false);

        // Keep our best individual
        if (elitism) {
            newPopulation.saveIndividual(0, popo.getfittest());
           // newPopulation.saveIndividual(1,popo.getSecondfittest());
        }

        int elitismOffset;
        if (elitism) {
            elitismOffset = 1;
        } else {
            elitismOffset = 0;
        }

        // crossover
        for (int i = elitismOffset; i < popo.getPopulationSize(); i++) {
            Individual individual = tournamentSelection(p , popo);
            Individual individual2 = tournamentSelection(p, popo);
            Individual newIndiv = crossover(individual, individual2,p);
            newPopulation.saveIndividual(i, newIndiv);
        }

        // Mutate population
        for (int i = elitismOffset; i < newPopulation.getPopulationSize(); i++) {
            mutate(newPopulation.getIndividual(i));
        }

        return newPopulation;
    }


    //selection
    public static Individual tournamentSelection( PricingProblem p , Population popo) {
        // Create a tournament population
        Population tournament = new Population(tournamentSize,p,false);
        // For each place in the tournament get a random individual
        for (int i = 0; i < tournamentSize; i++) {
            int randomId = (int) ((Math.random() * popo.getPopulationSize()) ) ;
            tournament.saveIndividual(i, popo.getIndividual(randomId));
        }

        Individual fittest = tournament.getfittest();
        return fittest;
    }

    //crossover
    public static Individual crossover(Individual indiv1, Individual indiv2 , PricingProblem p) {
        Individual newSol = new Individual(p);
        // Loop through genes
        for (int i = 0; i < indiv1.size(); i++) {
            // Crossover
            if (Math.random() <= uniformRate) {
                newSol.setGene(i, indiv1.getGene(i));
            } else {
                newSol.setGene(i, indiv2.getGene(i));
            }
        }
        return newSol;
    }

    //mutation
    public static void mutate(Individual individual) {
        // Loop through genes
        for (int i = 0; i < individual.size(); i++) {
            if (Math.random() <= mutationRate) {
                // Create random gene
                double max = 10.0;
                double min = 0.01;
                double gene = (Math.random() * (max - min) ) + min  ;
                individual.setGene(i, gene);
            }
        }
    }


}
